import './App.css';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import React, { Component } from 'react';
import { ToastContainer, toast } from 'mdbreact';
import { logoutUser, setCurrentUser } from './actions/authActions';

import FullPageWrapper from './components/layout/FullPageWrapper';
import NavbarFixed from './components/layout/NavbarFixed';
import NotFound from './components/layout/pages/NotFound';
import PropTypes from 'prop-types';
import ResetPassword from './components/layout/pages/ResetPassword';
import Strapi from 'strapi-sdk-javascript';
import ToastNotification from './components/layout/notification/ToastNotification';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import store from './store';
import { withTranslation } from 'react-i18next';

const strapi = new Strapi(process.env.REACT_APP_API_URL);

if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  strapi
    .getEntry('users', decoded.id)
    .then(res => {
      store.dispatch(setCurrentUser(res));
    })
    .catch(err => {
      store.dispatch(setCurrentUser({}));
    });
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/';
  }
}

class App extends Component {

  componentDidMount() {
    document.body.classList.add('scrollbar', 'scrollbar-warning');

    document
      .querySelectorAll([
        '.contact',
        '.toolkit',
        '.approach',
        '.contact .fp-tableCell',
        '.toolkit .fp-tableCell',
        '.approach .fp-tableCell'
      ])
      .forEach(function (element) {
        element.classList.add('section-auto-height');
      });
  }

  componentWillUnmount() {
    clearInterval(this.toastNotificationInterval);
  }

  render() {
    /* Register Toast Start */
    /*          if (localStorage.jwtToken) {
      toast.dismiss();
    }
    else {
      setTimeout(() => {
        if (
          !toast.isActive(this.oneTimeToast) &&
          !toast.isActive(this.repetitiveToast) &&
          !this.props.auth.isAuthenticated
        ) {
          this.oneTimeToast = toast(<ToastNotification />, {
            className: 'elegant-color z-depth-3 cursor-default',
            type: 'info',
            position: 'bottom-right',
            autoClose: 25000,
            draggable: true,
            closeOnClick: false,
            pauseOnHover: true
          });
        }
      }, 30000);

      this.toastNotificationInterval = setInterval(() => {
        if (
          !toast.isActive(this.oneTimeToast) &&
          !toast.isActive(this.repetitiveToast) &&
          !this.props.auth.isAuthenticated
        ) {
          this.repetitiveToast = toast(<ToastNotification />, {
            className: 'elegant-color z-depth-3 cursor-default',
            type: 'info',
            position: 'bottom-right',
            autoClose: 60000,
            draggable: true,
            closeOnClick: false,
            pauseOnHover: true
          });
        }
      }, 240000);
    }  */
    /* Register Toast End */

    /* Training Cancellation Toast Start */

    this.toastNotificationInterval = setInterval(() => {
      if (
        !toast.isActive(this.oneTimeToast) &&
        !toast.isActive(this.repetitiveToast)
      ) {
        this.repetitiveToast = toast(<ToastNotification />, {
          className: 'urban-training-toast z-depth-3 cursor-default',
          type: 'info',
          position: 'bottom-right',
          autoClose: false,
          draggable: true,
          closeOnClick: false,
          pauseOnHover: true,
          closeButton: false
        });
      }
    }, 2000);

    /* Training Cancellation Toast End */

    /* Training Toast Start */
    /*     setTimeout(() => {
      if (
        !toast.isActive(this.oneTimeToast) &&
        !toast.isActive(this.repetitiveToast)
      ) {
        this.oneTimeToast = toast(<ToastNotification />, {
          className: 'urban-training-toast z-depth-3 cursor-default',
          type: 'info',
          position: 'bottom-right',
          autoClose: 30000,
          draggable: true,
          closeOnClick: false,
          pauseOnHover: true
        });
      }
    }, 2000);

    this.toastNotificationInterval = setInterval(() => {
      if (
        !toast.isActive(this.oneTimeToast) &&
        !toast.isActive(this.repetitiveToast)
      ) {
        this.repetitiveToast = toast(<ToastNotification />, {
          className: 'urban-training-toast z-depth-3 cursor-default',
          type: 'info',
          position: 'bottom-right',
          autoClose: 30000,
          draggable: true,
          closeOnClick: false,
          pauseOnHover: true
        });
      }
    }, 90000); */
    /* Training Toast End */

    /* NDA Toast Start */
    /*     if (localStorage.jwtToken) {
      toast.dismiss();
    }
    else {
      setTimeout(() => {
        if (
          !toast.isActive(this.oneTimeToast) &&
          !toast.isActive(this.repetitiveToast) &&
          !this.props.auth.isAuthenticated
        ) {
          this.oneTimeToast = toast(<ToastNotification />, {
            className: 'urban-training-toast z-depth-3 cursor-default',
            type: 'info',
            position: 'bottom-right',
            autoClose: 30000,
            draggable: true,
            closeOnClick: false,
            pauseOnHover: true
          });
        }
      }, 2000);

      this.toastNotificationInterval = setInterval(() => {
        if (
          !toast.isActive(this.oneTimeToast) &&
          !toast.isActive(this.repetitiveToast) &&
          !this.props.auth.isAuthenticated
        ) {
          this.repetitiveToast = toast(<ToastNotification />, {
            className: 'urban-training-toast z-depth-3 cursor-default',
            type: 'info',
            position: 'bottom-right',
            autoClose: 30000,
            draggable: true,
            closeOnClick: false,
            pauseOnHover: true
          });
        }
      }, 90000);
    } */
    /* NDA Toast End */

    return (
      <BrowserRouter>
        <div
          className={`App ${!this.props.t('language.isRTL')
            ? 'font-montserratEn'
            : 'font-montserratAr'} `}>
          <NavbarFixed />
          <Switch>
            <Route exact path='/' component={FullPageWrapper} />
            <Route
              exact
              path='/user/reset-password/:code'
              component={ResetPassword}
            />
            <Route component={NotFound} />
          </Switch>
          <div className='d-none d-sm-block'>
            <ToastContainer />
          </div>{' '}
        </div>
      </BrowserRouter>
    );
  }
}

App.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {})(withTranslation()(App));