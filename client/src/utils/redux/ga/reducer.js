import { SEND_EVENT_ARABIC, SEND_EVENT_ENGLISH, SEND_GOOGLE_ANALYTICS_EVENT } from './actions';

import { initialState } from './store'

// import { redux } from 'redux'
const redux = require('redux');

const createStore = redux.createStore;

const reducer = (state = initialState, action) => {
    let resultCallAction;

    if (action === undefined || action.action === undefined) {
        return;
    } 

    if (action.type === SEND_GOOGLE_ANALYTICS_EVENT) {
        resultCallAction = action.action.call(state, action.params.categoryGa, action.params.actionGa);
    } else if (action.type === SEND_EVENT_ENGLISH) {
        resultCallAction = action.action.call(state, action.params.categoryGa, action.params.actionGa);
    } else if (action.type === SEND_EVENT_ARABIC) {
        resultCallAction = action.action.call(state, action.params.categoryGa, action.params.actionGa);
    } else {
        resultCallAction = action.action.call(state);
    }

    return resultCallAction;
}

export const storeGa = createStore(reducer);