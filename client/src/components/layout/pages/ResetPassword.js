import React, { Component } from 'react';
import axios from 'axios';
import {
  Mask,
  View,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBBtn,
  MDBIcon,
  MDBContainer
} from 'mdbreact';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      passwordConf: '',
      code: this.props.match.params.code,
      success: '',
      errors: {}
    };
  }

  onChange = e => {
    this.setState({ ...this.state, [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const forgotPassswordData = {
      password: this.state.password,
      passwordConf: this.state.passwordConf
    };

    axios
      .post(
        `${process.env.REACT_APP_API_URL}/user/reset-password/${this.state.code}`,
        forgotPassswordData
      )
      .then(res => {
        this.setState({ success: res.data, errors: {} });
      })
      .catch(err => {
        this.setState({ errors: err.response.data.message, success: '' });
      });
  };

  render() {
    const { errors, success } = this.state;
    return (
      <div id="reset-password-view">
        <View>
          <Mask overlay="black-strong" className="d-flex flex-center">
            <MDBContainer>
              <MDBRow className="justify-content-center align-items-center">
                <MDBCol md="6">
                  <MDBCard>
                    <div className="header pt-3 peach-gradient">
                      <MDBRow className="justify-content-center align-items-center">
                        <h3 className="white-text my-3 py-3 font-weight-bold">
                          Reset Password
                        </h3>
                      </MDBRow>
                    </div>
                    <MDBCardBody className="mx-4 mt-4">
                      {success && (
                        <div className="bg-info text-white small text-center">
                          Your password has been successfully reset!
                        </div>
                      )}
                      {errors.code && (
                        <div className="bg-danger text-white small text-center">
                          {errors.code}
                        </div>
                      )}
                      <form noValidate onSubmit={this.onSubmit}>
                        <label className="grey-text font-small">
                          New Password
                        </label>
                        <input
                          type="password"
                          className={`form-control form-control-sm ${errors.password &&
                            'is-invalid'}`}
                          name="password"
                          value={this.state.password}
                          onChange={this.onChange}
                        />
                        {errors.password && (
                          <div className="invalid-feedback">
                            {errors.password}
                          </div>
                        )}
                        <br />

                        <label className="grey-text font-small">
                          Confirm New Password
                        </label>
                        <input
                          type="password"
                          className={`form-control form-control-sm ${errors.passwordConf &&
                            'is-invalid'}`}
                          name="passwordConf"
                          value={this.state.passwordConf}
                          onChange={this.onChange}
                        />
                        {errors.passwordConf && (
                          <div className="invalid-feedback">
                            {errors.passwordConf}
                          </div>
                        )}

                        <br />

                        <div className="text-center">
                          <MDBBtn type="submit" color="warning" outline>
                            Change Password
                            <MDBIcon icon="unlock-alt" className="ml-2" />
                          </MDBBtn>
                        </div>
                      </form>
                    </MDBCardBody>
                  </MDBCard>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </Mask>
        </View>
      </div>
    );
  }
}

export default ResetPassword;
