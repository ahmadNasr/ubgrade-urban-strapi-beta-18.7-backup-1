import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Container, Row, Col, MDBIcon } from 'mdbreact';
import eu from '../../../img/eu.svg';
import euAr from '../../../img/ar/eu.svg';

class About extends Component {
  render() {
    const { t } = this.props;
    return (
      <Container
        fluid
        className={`${!t('language.isRTL') ? 'text-justify' : 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}>
        <Row center className='blue-grey lighten-5'>
          <Col lg='7' md='8' sm='9' size='10'>
            <div className='mt-5 mb-2 mx-2'>
              <h2 className='h2-responsive text-center text-urban-dark-blue font-weight-bold pb-4 text-uppercase'>
                {t('about.paragraph.title')}
              </h2>
              <p className='font-weight-normal pb-3'>
                {t('about.paragraph.body.p1')}
              </p>
              <p className='font-weight-normal'>
                {t('about.paragraph.body.p2.p1')}
              </p>
              <div className='mx-2'>
                <ul
                  className={`list-unstyled ml-3 pb-3 ${!t('language.isRTL') &&
                    'text-left'}`}>
                  <li>
                    <p className='mb-2'>
                      <MDBIcon
                        fixed
                        icon='check-square'
                        className='text-urban-dark-blue mx-1 font-small'
                      />
                      {t('about.paragraph.body.p2.p2.i1')}
                    </p>
                  </li>
                  <li>
                    <p className='mb-2'>
                      <MDBIcon
                        fixed
                        icon='check-square'
                        className='text-urban-dark-blue mx-1 font-small'
                      />
                      {t('about.paragraph.body.p2.p2.i2')}
                    </p>
                  </li>
                  <li>
                    <p className='mb-2'>
                      <MDBIcon
                        fixed
                        icon='check-square'
                        className='text-urban-dark-blue mx-1 font-small'
                      />
                      {t('about.paragraph.body.p2.p2.i3')}
                    </p>
                  </li>
                </ul>
              </div>
            </div>
            <div className='mb-5 mt-1 mx-2'>
              <p className='font-weight-normal'>
                {t('about.paragraph.body.p3.p1')}
              </p>
              <Row center>
                <Col md='6' size='12'>
                  <div className='mx-2'>
                    <ul
                      className={`list-unstyled ml-3 pb-0 mb-0 ${!t(
                        'language.isRTL'
                      ) && 'text-left'}`}>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i1')}
                        </p>
                      </li>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i2')}
                        </p>
                      </li>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i3')}
                        </p>
                      </li>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i4')}
                        </p>
                      </li>
                    </ul>
                  </div>
                </Col>

                <Col md='6' size='12'>
                  <div className='mx-2'>
                    <ul
                      className={`list-unstyled ml-3 pb-1 ${!t(
                        'language.isRTL'
                      ) && 'text-left'}`}>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i5')}
                        </p>
                      </li>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i6')}
                        </p>
                      </li>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i7')}
                        </p>
                      </li>
                      <li>
                        <p className='mb-2'>
                          <MDBIcon
                            fixed
                            icon='check-square'
                            className='text-urban-dark-blue mx-1 font-small'
                          />
                          {t('about.paragraph.body.p3.p2.i8')}
                        </p>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <Row center className='z-depth-2'>
          <Col
            md='6'
            size='12'
            className='d-flex justify-content-center align-items-center urban-elegant-gray-4 p-0'>
            <div className='text-white px-4 m-5'>
              <h4 className='h4-responsive font-weight-normal pb-2 text-uppercase'>
                {t('about.learn-more.title')}
              </h4>
              <p className='font-small font-weight-normal m-0'>
                {t('about.learn-more.body')}
              </p>
            </div>
          </Col>
          <Col
            md='6'
            size='12'
            className='d-flex justify-content-center align-items-center white'>
            <div className='d-flex m-5 justify-content-center align-items-center'>
              <a
                className='disabled'
                href='//ec.europa.eu'
                target='_blank'
                rel='noopener noreferrer'>
                <img
                  src={!t('language.isRTL') ? eu : euAr}
                  className='p-2 learn-more-logo'
                  title='European Union'
                  alt='European Union Logo'
                />
              </a>
            </div>
          </Col>
        </Row>
        <Row center className='about-bg'>
          <Col
            md='6'
            size='12'
            className='d-flex justify-content-center align-items-start urban-dark-blue-opacity p-0'>
            <div className='text-white px-4 m-5'>
              <h4 className='h4-responsive font-weight-normal pb-2 text-uppercase'>
                {t('about.ack.title')}
              </h4>
              <p className='font-weight-normal font-small m-0'>
                {t('about.ack.body')}
              </p>
            </div>
          </Col>
          <Col
            md='6'
            size='12'
            className='d-flex justify-content-center align-items-start urban-dark-orange-opacity p-0'>
            <div className='text-white px-4 m-5'>
              <h4 className='h4-responsive font-weight-normal pb-2 text-uppercase'>
                {t('about.disc.title')}
              </h4>
              <p className='font-weight-normal font-small m-0'>
                {t('about.disc.body')}
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default withTranslation()(About);
