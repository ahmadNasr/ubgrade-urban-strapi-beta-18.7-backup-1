const isTextHaveCharacters = (text, arrayChars = ['<', '>', '/', '^', '{', '}', '[', ']', '\\', '~', '&', '|']) => {
  // result = new RegExp(arrayChars.join("|")).test(text);

  var result = false;
  if (arrayChars.some(function(v) { return text.indexOf(v) >= 0; })) {
    result = true;
  }
  return result;
}

module.exports = isTextHaveCharacters;