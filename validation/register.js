const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.username = !isEmpty(data.username) ? data.username : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.operationsDesc = !isEmpty(data.operationsDesc)
    ? data.operationsDesc
    : '';
  data.position = !isEmpty(data.position) ? data.position : '';
  data.organization = !isEmpty(data.organization) ? data.organization : '';
  data.country = !isEmpty(data.country) ? data.country : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.passwordConf = !isEmpty(data.passwordConf) ? data.passwordConf : '';

  if (!Validator.isLength(data.name, { min: 2, max: 30 }))
    errors.name = 'Name must be between 2 and 30 characters';

  if (!Validator.isLength(data.username, { min: 2, max: 30 }))
    errors.username = 'Username must be between 2 and 30 characters';

  if (!Validator.isEmail(data.email)) errors.email = 'Email field is invalid';

  if (!Validator.isLength(data.operationsDesc, { min: 3, max: 120 }))
    errors.operationsDesc =
      'Area of Operations must be between 3 and 120 characters';

  if (!Validator.isLength(data.position, { min: 2, max: 60 }))
    errors.position = 'Position Title must be between 2 and 60 characters';

  if (!Validator.isLength(data.organization, { min: 2, max: 30 }))
    errors.organization = 'Organization must be between 2 and 30 characters';

  if (!Validator.isLength(data.country, { min: 2, max: 30 }))
    errors.country = 'Country must be between 2 and 30 characters';

  if (!Validator.isLength(data.password, { min: 6, max: 30 }))
    errors.password = 'Password must be at least 6 characters';

  if (strapi.api.user.services.user.isHashed(data.password))
    errors.password = "Password can't contain the $ symbol more than twice";

  if (!Validator.isLength(data.passwordConf, { min: 6, max: 30 }))
    errors.passwordConf = 'Password confirmation must be at least 6 characters';

  if (!Validator.equals(data.password, data.passwordConf))
    errors.passwordConf = 'Passwords must match';

  if (Validator.isEmpty(data.name)) errors.name = 'Name field is required';

  if (Validator.isEmpty(data.username))
    errors.username = 'Username field is required';

  if (Validator.isEmpty(data.email)) errors.email = 'Email field is required';

  if (Validator.isEmpty(data.operationsDesc))
    errors.operationsDesc = 'Area of Operations field is required';

  if (Validator.isEmpty(data.position))
    errors.position = 'Position Title field is required';

  if (Validator.isEmpty(data.organization))
    errors.organization = 'Organization field is required';

  if (Validator.isEmpty(data.country))
    errors.country = 'Country field is required';

  if (Validator.isEmpty(data.password))
    errors.password = 'Password field is required';

  if (Validator.isEmpty(data.passwordConf))
    errors.passwordConf = 'Password confirmation field is required';

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
